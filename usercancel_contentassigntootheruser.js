/**
 * @file
 * Show/hide user list on change of radio button.
 */

Drupal.behaviors.usercancel_contentassigntootheruser = {
  attach: function (context, settings) {
    'use strict';
    if (jQuery('#edit-user-cancel-method--6').is(':checked')) {
      jQuery('#userlist').show();
    }
    else {
      jQuery('#userlist').hide();
    }
    jQuery('input[name=user_cancel_method]:radio').change(function () {
      if (this.value === 'user_cancel_reassign_to_otheruser') {
        jQuery('#rolelist').show();
        jQuery('#userlist').show();
      }
      else {
        jQuery('#rolelist').hide();
        jQuery('#userlist').hide();
      }
    });
  }
};
