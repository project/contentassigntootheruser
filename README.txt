This module gives a feature to assign the content of a user to another user
If you are going to cancel the account of user.

Steps:
1. Enable the module.
2. You will have one more option(5th) with the list of cancel options.
3. Option text will be "Disable the account and transfer its content
to another user.".
4. If you want to assign the content to specific user, choose the role first,
 then select another user and cancel the account of selected user.
